/* Calculates the cumulative time to live from an
* array of requests.
* 
* param requests - Array containing requests 
*                  with startedAt and time to
*                  live (ttl)
*/
function calcCumulativeTTL (requests) {
    let max = 0,
    min = requests[0].startedAt,
    result = 0;
    
    /* Iterate over requests and find the first one to
    * start and the last one to finish.
    */
    for (let req of requests) {
        if (req.startedAt < min) min = req.startedAt;
        
        let reqTotal = (req.startedAt + req.ttl);
        if (reqTotal > max) max = reqTotal;
    }
	
    // Returns the difference between them
    return max - min;
}


let requests = [
    {requestId: 'poiax',  startedAt: 1489744808, ttl: 8},
    {requestId: 'kdfhd',  startedAt: 1489744803, ttl: 3},
    {requestId: 'uqwyet', startedAt: 1489744806, ttl: 12}, 
    {requestId: 'qewaz',  startedAt: 1489744810, ttl: 1}
]

console.log(calcCumulativeTTL(requests));