const fs = require('fs');

// Initialize the collections for each type of polygon
let triangles = [],
squares = [],
rectangles = [],
others = [];

// Listener to read each line on a file stream
// Better to handle large files, as it doesn't 
// read the whole file on a single string
// Reads each line and classify the polygon in it
function readLines(input, func) {
      let currentLine = '';
      
      input.on('data', function(data) {
            // reads the data available on the stream
            currentLine += data;
            // Checks if we got to a line break
            var index = currentLine.indexOf('\n');
            var last  = 0;
            while (index > -1) {
                  // Creates a substring until that index
                  var line = currentLine.substring(last, index);
                  // Updates the 'last' marker to the position 
                  // of the last character of the line
                  last = index + 1;
                  classifyPolygon(line);
                  // Updates index to the line break position
                  index = currentLine.indexOf('\n', last);
            }
            
            // Removes the line we read from the current line
            // ot continue the process
            currentLine = currentLine.substring(last);
      });
      
      input.on('end', function() {
            // When the streams ends, if we still have
            // something on the current Line, we call
            // our classifier again
            if (currentLine.length > 0) {
                  classifyPolygon(currentLine);
            }
            // Simple function to show results.
            // Wouldn't be there on production env
            showResult();
      });
}

/* Classifies a polygon in according to the 
* quantity and sizes of it sides.
* 
* param p - String containing the length of
*           the sides of a polygon e.g.
*           '1, 1, 1' or '2, 3, 2, 3'.
*/
function classifyPolygon(p) {
      // Transforms the string on an aarray of numbers
      // Trims to remove whitespace chars (e.g. \r)
      let sides = p.trim().split(',').map(Number);
      // Check for the number of sides.
      // If 3: It is a triangle
      if (sides.length == 3) 
            triangles.push(sides);
      // If 4: It can be a square, a rectangle or
      // something else
      else if (sides.length == 4) {
            // Here we find the number of different sizes
            // to classify the quadrilateral, as a square
            // has only one size on all sides and a 
            // rectangle has two different sizes between
            // all its sides.
            
            // Filter to remove duplicate numbers (sizes)
            // on the array.
            function unique(value, index, self) {
                  return self.indexOf(value) === index;
            }
            
            // Filter the different sides
            var differentSides = sides.filter(unique).length;
            
            // Classify the polygon according to the number
            // of different sides
            switch (differentSides) {
                  case 1:
                  squares.push(sides);
                  break;
                  case 2:
                  rectangles.push(sides);
                  break;
                  default:
                  others.push(sides);
                  break;
            }
      } 
      // If it isn't a triangle, a rectangle or a square, 
      // we classifyit as 'others'.
      else 
            others.push(sides);
}

// Prints all the subsets
function showResult() {    
      console.log('Triangles', triangles);
      console.log('Squares', squares);
      console.log('Rectangles', rectangles);
      console.log('Others', others);
}

// Creates a file stream from a file called polygons
// on the same folder as the program
let input = fs.createReadStream('polygons');
// Reads the stream 
readLines(input, classifyPolygon);