let a = [2, 4, 6, 8, 9, 15];
let b = ['4', '16', '64'];

var transform = (a) => {
    let b = 
        // Gets only the elements of 'a' we want
        a.filter(x => [2, 4, 8].indexOf(x) > -1)
        // Squares the number in each position
        .map(x => String(Math.pow(x, 2)));
    // Logs the result as required
    console.log(b);
    // Returns the transformed array
    return b;
}

// Call the function
transform(a);
